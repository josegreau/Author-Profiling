#!/bin/bash

counter=0
for i in {1..5} 
do
seed=$RANDOM
    for n in 10 20 30 40 50 100 
    do
        for ngram in {2..8} 
        do
            python main.py en $n 20 $ngram $seed $counter
            let counter++
        done
    done
done

